import unittest
from Supplier import Supplier
from unittest.mock import Mock, patch
import pickle
from threading import Timer
import sys

class TestSupplier(unittest.TestCase):
    def setUp(self):
        self.port = 55000
        self.channel_name = 'channel1'
        self.channel_name_2 = 'channel2'
        self.message = 'message'

    def tearDown(self):
        pass
    
    @patch('Supplier.Supplier.get_available_port')
    def test_init_no_port_in_cdb(self, mock_get_available_port):
        '''
        [init] should get new available port and save to cdb
        '''
        with patch('Supplier.CentralDB') as mock_cdb:
            mock_cdb_client = Mock()
            mock_get_nc_port = Mock(side_effect=TypeError("Test"))
            mock_set_nc_port = Mock()
            mock_cdb.return_value = mock_cdb_client
            mock_cdb_client.get_nc_port = mock_get_nc_port
            mock_cdb_client.set_nc_port = mock_set_nc_port
            mock_get_available_port.return_value = self.port

            with patch('Supplier.zmq') as mock_zmq:
                # mock zmq functions
                context = mock_zmq.Context.return_value
                mock_socket = Mock()
                mock_bind = Mock()

                context.socket = mock_socket
                socket = mock_socket.return_value
                socket.bind = mock_bind

                self.supplier = Supplier(self.channel_name)
                mock_get_available_port.assert_called_once()
                mock_set_nc_port.assert_called_once_with(self.channel_name, self.port)
                mock_bind.assert_called_once_with("tcp://*:{}".format(self.port))

    @patch('Supplier.Supplier.get_available_port')
    def test_init_port_exist_in_cdb(self, mock_get_available_port):
        '''
        [init] should bind port gotten from cdb to zmq socket
        '''
        with patch('Supplier.CentralDB') as mock_cdb:
            mock_cdb_client = Mock()
            mock_cdb.return_value = mock_cdb_client
            mock_get_nc_port = Mock(return_value=self.port)
            mock_set_nc_port = Mock()
            mock_cdb_client.set_nc_port = mock_set_nc_port
            mock_cdb_client.get_nc_port = mock_get_nc_port

            with patch('Supplier.zmq') as mock_zmq:
                # mock zmq functions
                context = mock_zmq.Context.return_value
                mock_socket = Mock()
                mock_bind = Mock()

                context.socket = mock_socket
                socket = mock_socket.return_value
                socket.bind = mock_bind

                self.supplier = Supplier(self.channel_name)
                self.assertEqual(mock_get_available_port.call_count, 0)
                self.assertEqual(mock_set_nc_port.call_count, 0)
                mock_bind.assert_called_once_with("tcp://*:{}".format(self.port))

    def test_publish_event(self):
        '''
        [publish_event] should publish event with channel name and pickled message
        '''
        with patch('Supplier.CentralDB') as mock_cdb:
            mock_cdb_client = Mock()
            mock_cdb.return_value = mock_cdb_client
            mock_get_nc_port = Mock(return_value=self.port)
            mock_set_nc_port = Mock()
            mock_cdb_client.set_nc_port = mock_set_nc_port
            mock_cdb_client.get_nc_port = mock_get_nc_port

            with patch('Supplier.zmq') as mock_zmq:
                # mock zmq functions
                context = mock_zmq.Context.return_value
                mock_socket = Mock()
                mock_bind = Mock()
                mock_send_multipart = Mock()

                context.socket = mock_socket
                socket = mock_socket.return_value
                socket.bind = mock_bind
                socket.send_multipart = mock_send_multipart

                self.supplier = Supplier(self.channel_name)
                self.supplier.publish_event(self.message)

                # Expected Result
                mock_send_multipart.assert_called_with([
                    bytes(self.channel_name, 'utf-8'), 
                    pickle.dumps(self.message)
                ])

    @patch('Supplier.zmq')
    def test_get_available_port(self, mock_zmq ):
        '''
        [get_available_port] should increase port number until get the available and not reserved one
        '''
        port_start = 50000
        max_port = 50020
        with patch('Supplier.CentralDB') as mock_cdb:
            mock_cdb_client = Mock()
            mock_get_all_nc_port = Mock(return_value=[b'channel2'])
            mock_get = Mock(return_value=port_start)
            mock_cdb.return_value = mock_cdb_client
            mock_cdb_client.get = mock_get
            mock_cdb_client.get_all_nc_port = mock_get_all_nc_port

            with patch('Supplier.socket.socket') as mock_socket:
                sock = Mock()
                mock_socket.return_value = sock

                self.supplier = Supplier(self.channel_name)
                mock_bind = Mock(side_effect=[OSError("Test"), True])
                sock.bind = mock_bind
                result = self.supplier.get_available_port(port=port_start, max_port=max_port)

                self.assertEqual(mock_bind.call_count, 2)
                self.assertEqual(result, port_start + 2)

    @patch('Supplier.zmq')
    def test_get_available_port_cdb_empty(self, mock_zmq ):
        '''
        [get_available_port] should increase port when cdb empty
        '''
        port_start = 50000
        max_port = 50020
        with patch('Supplier.CentralDB') as mock_cdb:
            mock_cdb_client = Mock()
            mock_get_all_nc_port = Mock(return_value=[])
            mock_get = Mock(return_value=port_start)
            mock_cdb.return_value = mock_cdb_client
            mock_cdb_client.get = mock_get
            mock_cdb_client.get_all_nc_port = mock_get_all_nc_port

            with patch('Supplier.socket.socket') as mock_socket:
                sock = Mock()
                mock_socket.return_value = sock

                self.supplier = Supplier(self.channel_name)
                mock_bind = Mock(side_effect=[OSError("Test"), True])
                sock.bind = mock_bind
                result = self.supplier.get_available_port(port=port_start, max_port=max_port)

                self.assertEqual(mock_bind.call_count, 2)
                self.assertEqual(result, port_start + 1)

    @patch('Supplier.CentralDB')
    @patch('Supplier.zmq')
    def test_get_available_port_no_port_available(self, mock_cdb, mock_zmq ):
        '''
        [get_available_port] should raise IOError when port in range is not available
        '''
        port_start = 50000
        max_port = 50001
        with patch('Supplier.socket.socket') as mock_socket:
            sock = Mock()
            mock_socket.return_value = sock

            self.supplier = Supplier(self.channel_name)
            mock_bind = Mock(side_effect=[OSError("Test"), OSError("Test"), OSError("Test"), True])
            sock.bind = mock_bind

            self.assertRaises(
                IOError, 
                self.supplier.get_available_port,
                port=port_start,
                max_port=max_port
            )
        