import unittest
from Consumer import Consumer
from unittest.mock import Mock, patch
import pickle
from threading import Timer

class TestConsumer(unittest.TestCase):
    def setUp(self):
        self.channel_name = 'AntennaStatusChannel'
        self.channel_name_2 = 'AntennaArrivedChannel'
        self.message = 'message'
        self.port = 55000
        self.ip = 'localhost'

    def tearDown(self):
        pass


    def test_init_with_port_in_cdb(self):
        '''
        [init] should connect to channel with port from cdb
        '''
        with patch('Consumer.PySimpleClient') as mock_sc:
            # mock PySimpleClient functions
            sc = mock_sc.return_value
            mock_get_component = Mock()
            mock_release_component = Mock()

            sc.getComponent = mock_get_component
            sc.releaseComponent = mock_release_component
            component = mock_get_component.return_value
            
            mock_get_ip = Mock()
            mock_get_ip.return_value = 'localhost'
            component.get_ip = mock_get_ip

            with patch('Consumer.CentralDB') as mock_cdb:
                mock_cdb_client = Mock()
                mock_get_nc_port = Mock(return_value=self.port)
                mock_set_nc_port = Mock()
                mock_cdb.return_value = mock_cdb_client
                mock_cdb_client.get_nc_port = mock_get_nc_port
                mock_cdb_client.set_nc_port = mock_set_nc_port

                with patch('Consumer.zmq') as mock_zmq:
                    # mock zmq functions
                    context = mock_zmq.Context.return_value
                    mock_socket = Mock()
                    mock_connect = Mock()

                    context.socket = mock_socket
                    socket = mock_socket.return_value
                    socket.connect = mock_connect


                    self.consumer = Consumer(self.channel_name)
                    mock_get_nc_port.assert_called_once()
                    self.assertEqual(mock_set_nc_port.call_count, 0)
                    mock_connect.assert_called_once_with("tcp://{}:{}".format(self.ip, self.port))

    @patch('Consumer.Consumer.get_available_port')
    def test_init_no_port_in_cdb(self, mock_get_available_port):
        '''
        [init] should connect to channel with port from get_available port function
        '''
        with patch('Consumer.PySimpleClient') as mock_sc:
            # mock PySimpleClient functions
            sc = mock_sc.return_value
            mock_get_component = Mock()
            mock_release_component = Mock()

            sc.getComponent = mock_get_component
            sc.releaseComponent = mock_release_component
            component = mock_get_component.return_value
            
            mock_get_ip = Mock()
            mock_get_ip.return_value = 'localhost'
            component.get_ip = mock_get_ip

            with patch('Consumer.CentralDB') as mock_cdb:
                mock_cdb_client = Mock()
                mock_get_nc_port = Mock(side_effect=TypeError("Test"))
                mock_set_nc_port = Mock()
                mock_get_available_port.return_value = self.port
                mock_cdb.return_value = mock_cdb_client
                mock_cdb_client.get_nc_port = mock_get_nc_port
                mock_cdb_client.set_nc_port = mock_set_nc_port

                with patch('Consumer.zmq') as mock_zmq:
                    # mock zmq functions
                    context = mock_zmq.Context.return_value
                    mock_socket = Mock()
                    mock_connect = Mock()

                    context.socket = mock_socket
                    socket = mock_socket.return_value
                    socket.connect = mock_connect


                    self.consumer = Consumer(self.channel_name)
                    mock_get_nc_port.assert_called_once()
                    mock_get_available_port.assert_called_once()
                    mock_set_nc_port.assert_called_once()
                    mock_connect.assert_called_once_with("tcp://{}:{}".format(self.ip, self.port))


    @patch('Consumer.zmq')
    def test_get_available_port(self, mock_zmq ):
        '''
        [get_available_port] should increase port number until get the available and not reserved one
        '''
        port_start = 50000
        max_port = 50020
        with patch('Consumer.PySimpleClient') as mock_sc:
            # mock PySimpleClient functions
            sc = mock_sc.return_value
            mock_get_component = Mock()
            mock_release_component = Mock()

            sc.getComponent = mock_get_component
            sc.releaseComponent = mock_release_component
            component = mock_get_component.return_value
            
            mock_get_ip = Mock()
            mock_get_ip.return_value = 'localhost'
            component.get_ip = mock_get_ip

            with patch('Consumer.CentralDB') as mock_cdb:
                mock_cdb_client = Mock()
                mock_get_all_nc_port = Mock(return_value=[b'AntennaArrivedChannel'])
                mock_get = Mock(return_value=port_start)
                mock_cdb.return_value = mock_cdb_client
                mock_cdb_client.get = mock_get
                mock_cdb_client.get_all_nc_port = mock_get_all_nc_port

                with patch('Consumer.socket.socket') as mock_socket:
                    sock = Mock()
                    mock_socket.return_value = sock
                    self.consumer = Consumer(self.channel_name)
                    mock_bind = Mock(side_effect=[OSError("Test"), True])
                    sock.bind = mock_bind
                    result = self.consumer.get_available_port(port=port_start, max_port=max_port)

                    self.assertEqual(mock_bind.call_count, 2)
                    self.assertEqual(result, port_start + 2)

    @patch('Consumer.zmq')
    def test_get_available_port_cdb_empty(self, mock_zmq ):
        '''
        [get_available_port] should increase port when cdb empty
        '''
        port_start = 50000
        max_port = 50020
        with patch('Consumer.PySimpleClient') as mock_sc:
            # mock PySimpleClient functions
            sc = mock_sc.return_value
            mock_get_component = Mock()
            mock_release_component = Mock()

            sc.getComponent = mock_get_component
            sc.releaseComponent = mock_release_component
            component = mock_get_component.return_value
            
            mock_get_ip = Mock()
            mock_get_ip.return_value = 'localhost'
            component.get_ip = mock_get_ip
            with patch('Consumer.CentralDB') as mock_cdb:
                mock_cdb_client = Mock()
                mock_get_all_nc_port = Mock(return_value=[])
                mock_get = Mock(return_value=port_start)
                mock_cdb.return_value = mock_cdb_client
                mock_cdb_client.get = mock_get
                mock_cdb_client.get_all_nc_port = mock_get_all_nc_port

                with patch('Consumer.socket.socket') as mock_socket:
                    sock = Mock()
                    mock_socket.return_value = sock

                    self.consumer = Consumer(self.channel_name)
                    mock_bind = Mock(side_effect=[OSError("Test"), True])
                    sock.bind = mock_bind
                    result = self.consumer.get_available_port(port=port_start, max_port=max_port)

                    self.assertEqual(mock_bind.call_count, 2)
                    self.assertEqual(result, port_start + 1)

    @patch('Consumer.CentralDB')
    @patch('Consumer.zmq')
    def test_get_available_port_no_port_available(self, mock_cdb, mock_zmq ):
        '''
        [get_available_port] should raise IOError when port in range is not available
        '''
        port_start = 50000
        max_port = 50001
        with patch('Consumer.PySimpleClient') as mock_sc:
            # mock PySimpleClient functions
            sc = mock_sc.return_value
            mock_get_component = Mock()
            mock_release_component = Mock()
            mock_disconnect = Mock()

            sc.getComponent = mock_get_component
            sc.releaseComponent = mock_release_component
            sc.disconnect = mock_disconnect
            component = mock_get_component.return_value
            
            mock_get_ip = Mock()
            mock_get_ip.return_value = 'localhost'
            component.get_ip = mock_get_ip
            with patch('Consumer.socket.socket') as mock_socket:
                sock = Mock()
                mock_socket.return_value = sock

                self.consumer = Consumer(self.channel_name)
                mock_bind = Mock(side_effect=[OSError("Test"), OSError("Test"), OSError("Test"), True])
                sock.bind = mock_bind

                self.assertRaises(
                    IOError, 
                    self.consumer.get_available_port,
                    port=port_start,
                    max_port=max_port
                )

    @patch('Consumer.threading')
    @patch('Consumer.PySimpleClient')
    @patch('Consumer.CentralDB')
    @patch('Consumer.zmq')
    @patch('Consumer.Consumer.connect')
    def test_add_subscription(self,mock_threading, mock_sc, mock_cdb, mock_zmq, mock_connect):
        '''
        [add_subscription] should set consumer active event and start new consumer thread
        '''
        with patch('Consumer.threading.Thread') as mock_thread:

            mock_handler = Mock()
            self.consumer = Consumer(self.channel_name)

            mock_set_event = Mock()
            self.consumer.consumer_active.set = mock_set_event

            # Test Function
            self.consumer.add_subscription(mock_handler)

            # Expected result
            mock_set_event.assert_called_once()
            mock_thread.assert_called_with(
                target=self.consumer.connect,
                args=[mock_handler]
            )
            mock_thread().start.assert_called()

    @patch('Consumer.PySimpleClient')
    @patch('Consumer.CentralDB')
    def test_connect(self, mock_sc, mock_cdb):
        '''
        [connect] handler function should be called with message from socket when receive
        '''

        with patch('Consumer.zmq') as mock_zmq:
            # mock zmq functions
            context = mock_zmq.Context.return_value
            mock_socket = Mock()

            context.socket = mock_socket
            socket = mock_socket.return_value
            
            mock_recv_multipart = Mock()
            mock_recv_multipart.return_value = [
                bytes(self.channel_name, 'utf-8'), 
                pickle.dumps(self.message)
            ]

            socket.recv_multipart = mock_recv_multipart

            self.consumer = Consumer(self.channel_name)
            
            timer = Timer(0.5, self.consumer.consumer_active.clear)
            self.consumer.consumer_active.set()
            timer.start()

            # Test Function
            mock_handler = Mock()
            self.consumer.connect(mock_handler)

            # Expected result
            mock_handler.assert_called_with(self.message)


    @patch('Consumer.PySimpleClient')
    @patch('Consumer.CentralDB')
    @patch('Consumer.zmq')
    def test_disconnect(self, mock_sc, mock_cdb, mock_zmq):
        '''
        [disconnect] should clear consumer_active event
        '''

        # Setup
        self.consumer = Consumer(self.channel_name)
        mock_clear_event = Mock()
        self.consumer.consumer_active.clear = mock_clear_event

        # Test Function
        self.consumer.disconnect()

        # Expected result
        mock_clear_event.assert_called_once()




            
            
