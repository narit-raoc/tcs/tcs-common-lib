import logging
import zmq
import time
import pickle
import socket
from CentralDB import CentralDB 

class Supplier:
    
    def __init__(self, channel):
        '''
        Constructor.

        Params:
        - channelName is the channel name
        '''
        self.channel = channel

        # Logger for this Supplier
        self.logger = logging.getLogger("Supplier {}".format(channel))

        try:
            # Get port from cdb
            self.cdb = CentralDB()
            self.port = self.cdb.get_nc_port(self.channel)
        except TypeError as e:
            # No port is recorded to CDB
            # Get new available port
            self.logger.debug('No port is set to CDB for channel {}'.format(self.channel))
            self.logger.debug('get new available port')
            self.port = self.get_available_port()

            # Save the port number to cdb so clients know which port to connect
            self.logger.debug('save new port {} to CDB for channel {}'.format(self.port, self.channel))
            self.cdb.set_nc_port(self.channel, self.port)

        try:
            # bind zmq
            self.context = zmq.Context()
            self.socket = self.context.socket(zmq.PUB)
            self.socket.bind("tcp://*:{}".format(self.port))
        except Exception as e:
            self.logger.error(e)
            raise e

        self.logger.info('Setting up notification channel')
        self.logger.info('channel: {}'.format(channel))
        self.logger.info('port: {}'.format(self.port))

    def __del__(self):
        self.disconnect()

    def disconnect(self):
        try:
            self.socket.close()
            self.context.destroy()
        except Exception as e:
            self.logger.error(e)

    def publish_event(self, message):
        pickled_message = pickle.dumps(message)
        self.socket.send_multipart([bytes(self.channel, 'utf-8'), pickled_message])

    def get_available_port(self, host='127.0.0.1', port=50000, max_port=65535):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        all_channel = self.cdb.get_all_nc_port()
        reserved_port = []

        for key in all_channel:
            try:
                p = self.cdb.get(key.decode('utf-8'))
                reserved_port.append(int(p))
            except Exception as e:
                print(e)

        if len(reserved_port) > 0:
            max_reserved_port = max(reserved_port)
            port = max_reserved_port + 1
            
        while port <= max_port:
            try:
                print('port', port)
                sock.bind((host, port))
                sock.close()
                return port
            except OSError:
                port += 1
        raise IOError('no free ports')

if __name__ == "__main__":
    try:
        channel = 'AntennaStatusChannel'
        supplier = Supplier(channel)
        print('channel: {}'.format(supplier.channel))
        print('port: {}'.format(supplier.port))
        for i in range(100):
            messagedata = 'message data {}'.format(i)
            print(messagedata)
            supplier.publish_event(messagedata)
            time.sleep(1)
    except KeyboardInterrupt:
        print('KeyboardInterrupt')